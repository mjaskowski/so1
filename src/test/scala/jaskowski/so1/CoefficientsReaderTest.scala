package jaskowski.so1

import org.scalatest.{Matchers, BeforeAndAfter, FlatSpec}
import java.io.StringReader

class CoefficientsReaderTest extends FlatSpec with BeforeAndAfter with Matchers {

  it must "read all coefficients" in {
    val coefficients =
      """"","n","alpha_ASC_1","alpha_ASC_2","alpha_ASC_3","beta"
        |"1",1,1.0,2.0,3.0,-66.0""".stripMargin

    val result = new CoefficientsReader(3).read(new StringReader(coefficients))

    result should be (Map(Person(1) -> Coefficients(List(1.0,2.0, 3.0), -66.0)))
  }

  it must "substitute coefficient not given with a 0" in {
    val coefficients =
      """"","n","alpha_ASC_1","alpha_ASC_3","beta"
        |"1",1,1.0,3.0,-66.0""".stripMargin

    val result = new CoefficientsReader(3).read(new StringReader(coefficients))

    result should be (Map(Person(1) -> Coefficients(List(1.0,0.0, 3.0), -66.0)))
  }
}
