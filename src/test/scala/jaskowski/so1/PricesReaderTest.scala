package jaskowski.so1

import org.scalatest.{Matchers, BeforeAndAfter, FlatSpec}
import java.io.StringReader

class PricesReaderTest extends FlatSpec with BeforeAndAfter with Matchers {

  it must "read price with product number decremented by 1" in {
    val prices =
      """"","n","t","j","not important","Price"
        |"1",1,2,3,"xxx",12.78""".stripMargin

    val result = new PricesReader().read(new StringReader(prices))

    result should be (Map((Person(1),Time(2)) -> List((Product(2), 12.78))))
  }

  it must "read several prices with product number decremented by 1" in {
    val prices =
      """"","n","t","j","not important","Price"
        |"1",1,2,3,"xxx",12.78
        |"9",9,8,7,"yyy",9.99""".stripMargin

    val result = new PricesReader().read(new StringReader(prices))

    result should be (Map(
      (Person(1),Time(2)) -> List((Product(2), 12.78)),
      (Person(9),Time(8)) -> List((Product(6), 9.99))
    ))
  }
 }
