package jaskowski.so1

import org.scalatest._

class PurchaseProbabilitiesTest extends FlatSpec with BeforeAndAfter with Matchers {

  "calculated probabilities" should "sum to 1" in {
    val alphas = List(-2.98672022922387,2.24392558484532,-0.624671158710732,0.0177990839217826,-0.313632238640594,-0.639920797241709,1.38344715723932,3.61481586933791,-1.23256969535792,0.394103728651321,-1.68727377131018,-0.395755007596988,0.333049701382445,-1.09350848188629,-2.99302705951354,3.36240033793225,-1.01412295877115,3.42692564664816,2.40965410458769)
    val beta = -2.04036181358299
    val prices = Map(Product(0) -> 1.29,Product(1) -> 1.79, Product(2) -> 1.79,Product(3) -> 1.79,Product(4) -> 1.59,Product(5) -> 2.29,Product(6) -> 1.79,Product(7) -> 2.39)

    val purchaseProbabilities = new PurchaseProbabilities(alphas = alphas, beta = beta)

    val result = purchaseProbabilities.calculateFor(prices.toList)

    val sumOfProbabilities = result.map(_.probability).reduce(_+_)

    sumOfProbabilities should equal (1.0 +- (0.00001))
  }

  "product with lower usability" should "have lower probability of being bought for negative beta" in {
    val purchaseProbabilities = new PurchaseProbabilities(alphas = List(1.99, 2), beta = -2)

    val purchaseProb = purchaseProbabilities.calculateFor(Map(Product(0) -> 1., Product(1) -> 1.).toList).map(toTuple).toMap

    purchaseProb(0) should be < purchaseProb(1)
  }

  "product with lower price" should "have higher probability of begin bought for negative beta" in {
    val purchaseProbabilities = new PurchaseProbabilities(alphas = List(-2, -2), beta = -2)

    val purchaseProb = purchaseProbabilities.calculateFor(Map(Product(0) -> 1.01, Product(1) -> 1.).toList).map(toTuple).toMap

    purchaseProb(0) should be < purchaseProb(1)
  }

  "among three products the one with highest usability" should "have highest probability of being bought" in {
    val purchaseProbabilities = new PurchaseProbabilities(alphas = List(-2, 0, 2), beta = -2)

    val purchaseProb = purchaseProbabilities.calculateFor(Map(Product(0) -> 0., Product(1) -> 1.01, Product(2) -> 2.01).toList).map(toTuple).toMap

    purchaseProb(0) should be > purchaseProb(1)
    purchaseProb(0) should be > purchaseProb(2)
  }

  def toTuple(ppp: ProductPurchaseProbability) = (ppp.product.id, ppp.probability)

}
