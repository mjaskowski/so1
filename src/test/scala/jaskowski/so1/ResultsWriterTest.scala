package jaskowski.so1

import org.scalatest.{Matchers, BeforeAndAfter, FlatSpec}
import java.io.StringWriter

class ResultsWriterTest extends FlatSpec with BeforeAndAfter with Matchers {


  it must "write header" in {
    val sw = new StringWriter();

    new ResultsWriter(sw).write(List())

    sw.toString should be
    """n,t,j,probability""".stripMargin
  }

  it must "write the result with product id + 1" in {
    val sw = new StringWriter();

    new ResultsWriter(sw).write(List(((Person(7), Time(9)), List(
      ProductPurchaseProbability(Product(0), 0.3),
      ProductPurchaseProbability(Product(1), 0.7)))))

    sw.toString should be
    """n,t,j,probability
      |7,9,1,0.3
      |7,9,2,0.7
    """.stripMargin
  }

  it must "write the result of several persons and times in the right order" in {
    val sw = new StringWriter();

    new ResultsWriter(sw).write(List(
      ((Person(7), Time(9)), List(ProductPurchaseProbability(Product(0), 0.3), ProductPurchaseProbability(Product(1), 0.7))),
      ((Person(6), Time(8)), List(ProductPurchaseProbability(Product(0), 0.4), ProductPurchaseProbability(Product(2), 0.6)))
    ))

    sw.toString should be
    """n,t,j,probability
      |6,8,1,0.4
      |6,8,3,0.6
      |7,9,1,0.3
      |7,9,2,0.7
    """.stripMargin
  }


}
