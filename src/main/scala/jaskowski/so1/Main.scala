package jaskowski.so1

import java.io.{FileReader, FileWriter}
import java.util.Calendar


object Main {

  type Price = Double
  type Alpha = Double
  type Beta = Double


  def main(args: Array[String]) {

    if (args.length != 3) {
      println(
        """Usage:
          |java -Xmx512M -jar so1-assembly-1.0.jar pathToCoefficients pathToPrices outputFile
        """.stripMargin)
      return
    }

    val coefficientsFileReader = new FileReader(args(0))
    val pricesFileReader = new FileReader(args(1))
    val resultsFileWriter = new FileWriter(args(2))

    println(Calendar.getInstance().getTime(), "Reading coefficients")
    val coefficientsForPerson: Map[Person, Coefficients] = new CoefficientsReader(20).read(coefficientsFileReader)
    println(Calendar.getInstance().getTime(), "Read coefficients")

    println(Calendar.getInstance().getTime(), "Reading prices")
    val pricesForPersonAtTime: Map[(Person, Time), List[(Product, Price)]] = new PricesReader().read(pricesFileReader);
    println(Calendar.getInstance().getTime(), "Read prices")

    val personAndTime: List[(Person, Time)] = pricesForPersonAtTime.keys.toList

    val before = Calendar.getInstance().getTimeInMillis()
    println(Calendar.getInstance().getTime(), "Calculation started")

    val results = for ((person, time) <- personAndTime) yield {
      ((person, time), coefficientsForPerson(person) match {
        case Coefficients(alphas, beta) =>
          new PurchaseProbabilities(alphas, beta).calculateFor(pricesForPersonAtTime((person, time)))
      })

    }
    println(Calendar.getInstance().getTime(), "Calculation ended")
    println("Calculation time: ", (Calendar.getInstance().getTimeInMillis() - before), "ms")

    new ResultsWriter(resultsFileWriter).write(results)
  }
}
