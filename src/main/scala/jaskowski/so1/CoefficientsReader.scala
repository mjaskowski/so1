package jaskowski.so1

import com.github.tototoshi.csv._
import java.io.Reader

class CoefficientsReader(numberOfProducts:Int) {

  val alphaNames: List[(Int, String)] = (1 until numberOfProducts+1).toList map (n => (n - 1, "alpha_ASC_" + n))

  def read(fileReader: Reader): Map[Person, Coefficients] = {
    val rows = CSVReader.open(fileReader).allWithHeaders()
    
    rows.map(m => {
      val alphas = alphaNames.map {
        case (index, name) => if (m.get(name).isDefined) (index, m(name).toDouble) else (index, 0.)
      }

      (Person(m("n").toInt), Coefficients(alphas.map(_._2), m("beta").toDouble))
    }).toMap
  }
}
