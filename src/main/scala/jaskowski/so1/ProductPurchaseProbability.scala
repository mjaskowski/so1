package jaskowski.so1

case class ProductPurchaseProbability(product: Product, probability: Double)
