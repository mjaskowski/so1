package jaskowski.so1

case class Coefficients(alphas: List[Double], beta: Double)
