package jaskowski.so1

import com.github.tototoshi.csv.CSVWriter
import java.io.Writer

class ResultsWriter(fileWriter: Writer) {
  val writer = CSVWriter.open(fileWriter)
  writer.writeRow(Seq("n", "t", "j", "probability"))

  def write(result: List[((Person, Time), List[ProductPurchaseProbability])]) = {
    writer.writeAll(
      (for ( x <- result; productPurchaseProbability <- x._2)
        yield (x._1._1.id, x._1._2.t, productPurchaseProbability.product.id + 1, productPurchaseProbability.probability))
      .sorted
      .map(tuple => List(tuple._1, tuple._2, tuple._3, tuple._4))
    )
  }
}
