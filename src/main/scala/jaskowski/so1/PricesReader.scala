package jaskowski.so1

import com.github.tototoshi.csv.CSVReader
import java.io.{Reader, File}

class PricesReader {

  def read(fileReader: Reader):Map[(Person, Time), List[(Product, Double)]] = {
    val rows = CSVReader.open(fileReader).allWithHeaders()

    (rows map { m =>
      val person = m("n")
      val time = m("t")
      val product = m("j")
      val price = m("Price")
      (Person(person.toInt), Time(time.toInt), Product(product.toInt - 1), price.toDouble)
    }).groupBy {
      case (person, time, _, _) => (person, time)
    }.map {
      case (personAndTime, pricesOfProducts) => (personAndTime, pricesOfProducts map {
        case (_, _, product, price) => (product, price)
      })
    }

  }
}
