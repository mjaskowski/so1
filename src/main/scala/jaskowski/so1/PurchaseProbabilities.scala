package jaskowski.so1


class PurchaseProbabilities(val alphas: List[Double], val beta: Double) {

  def calculateFor(prices: List[(Product,Double)]): List[ProductPurchaseProbability] = {
    val choiceSet: List[Int] = prices.map(_._1.id)

    val pricesChoiceSet = prices.sortBy(_._1.id).map(_._2)

    val alphasChoiceSet:List[Double] = {
      val choiceSetAsSet:Set[Int] = choiceSet.toSet
      alphas
        .zipWithIndex
        .filter { case (_,index) => choiceSetAsSet.contains(index) }
        .map(_._1)
    }

    val expUtilities = alphasChoiceSet
      .zip(pricesChoiceSet)
      .map(expUtility)


    val denominator = expUtilities.reduce(_+_)
    val probabilities = expUtilities.map(_ / denominator)

    choiceSet.zip(probabilities).map(x => ProductPurchaseProbability(Product(x._1), x._2))

  }

  private[this] def expUtility = (tuple: (Double, Double)) => scala.math.exp(tuple._1 + beta * tuple._2)
}
