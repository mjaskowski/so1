name := "so1"

version := "1.0"

scalaVersion := "2.10.3"

mainClass in Compile := Some("jaskowski.so1.Main")
    
libraryDependencies ++= Seq(
  "com.github.tototoshi" %% "scala-csv" % "1.0.0",
  "org.scalatest" %% "scalatest" % "2.0" % "test",
  "org.mockito" % "mockito-core" % "1.9.5" % "test",
  "junit" % "junit" % "4.11" % "test"
)
